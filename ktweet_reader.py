#!/usr/bin/python3
#! coding:utf-8
#
#

import lxml
from lxml import html 
from lxml import etree
import requests as req
import yaml
import codecs
import requests
from bs4 import BeautifulSoup
from datetime import datetime, timedelta, timezone
import argparse
import os

class tweet :
    """ 個別のツイートを入れるためのclass """
    def __init__ (self,s):
        """ インスタンス変数 """
        self.raw_string=s
        self.id=None
        self.is_retweeted=False
        self.is_pinned=False
        self.unixtime_str=""
        self.rfc_time_str=""
        self.created_time=None
        self.retweet_user_id=None
        self.text=""
        self.url=[]
        self.photo_url=[]

    def dump_yml (self):
        s="\n---\n"
        s=s+ ("RT: {}\n".format(self.retweet_user_id) if self.is_retweeted else "")
        s=s+"id: {}\n".format(self.id)
        s=s+"date: {}\n".format(self.rfc_time_str)
        if self.url:
            s=s+"url: {}\n".format(self.url)
        if self.photo_url:
            s=s+"photo: {}\n".format(self.photo_url)
        s=s+"""text: |\n  {}""".format((self.text).replace("\n","\n  "))

        return s
    
    def p(self):
        root=etree.fromstring(self.raw_string.replace("\n",""), etree.HTMLParser())
        self.unixtime_str = root.xpath('.//span[contains(@class, "_timestamp")]/@data-time')[0]
        jst=timezone(timedelta(hours=+9), 'JST')
        self.rfc_time_str = datetime.fromtimestamp(int(self.unixtime_str),jst).isoformat()
        self.id = root.xpath('.//div[contains(@class, "tweet")]/@data-item-id')[0]
        status_text=root.xpath('.//p[@class="TweetTextSize TweetTextSize--normal js-tweet-text tweet-text"]')
        t=html.tostring(status_text[0], method='text' ,encoding='utf-8').decode("utf-8")
        while t.startswith(" "):
            t=t[1:]
        while t.endswith(" "):
            t=t[:-1]
        self.text=t
        exp_url = status_text[0].xpath('.//a[contains(@class, "twitter-timeline-link")]/@data-expanded-url')
        if exp_url:
            self.url = exp_url
        self.is_retweeted= True if root.xpath('.//span[@class="js-retweet-text"]') else False
        if self.is_retweeted:
            self.retweet_user_id = root.xpath('.//a[contains(@class, "account-group")]/@href')[0].replace("/","")
        self.is_pinned = True if root.xpath('//*/div[contains(@class, "pinned") and contains(@class, "tweet")]') else False
        photo= root.xpath('.//div[contains(@class, "AdaptiveMedia")]//img/@src')
        if photo:
            self.photo_url = photo
        
    def pprint(self):
        print(BeautifulSoup(self.raw_string,features="lxml").prettify())

class BigA:
    """ Class doc """
    
    def __init__ (self):
        """ Class initialiser """
        self.maxid=None
        self.minid=None
        self.raw_string=""
        self.list_t=[]

    def from_file (self,filename,min_id=float("-Inf"),max_id=float("Inf")):
        f=open(filename,"r")
        self.raw_string=" ".join(f.read().split())
        root= etree.fromstring(self.raw_string, etree.HTMLParser())
        troot_list = root.xpath('//*/div[contains(@class, "tweet") and contains(@class, "js-stream-tweet")]')

        for t in troot_list:
            tt=lxml.html.tostring(t,encoding="utf-8").decode("utf-8")
            i=tweet(tt)
            i.p()
            if min_id<int(i.id)<max_id:
                if not i.is_pinned:
                    self.list_t.append(i)
        return self.list_t

    def fetch (self,uid,min_id=None,max_id=None):
        """ Function doc """
        def getraw(user,maxid=None):
            if maxid==None:
                url="https://twitter.com/i/profiles/show/"+user+"/timeline/tweets"
            else:
                url="https://twitter.com/i/profiles/show/"+user+"/timeline/tweets?max_position="+maxid
            res=req.get(url)
            if not res.ok:
                return ""
            if "" == res.json()["items_html"].strip():
                return ""
            else:
                if (not "" == res.json()["min_position"].strip()):
                    return res.json()['items_html'] + getraw(user,res.json()["min_position"])
            return ""
        self.raw_string=getraw(uid)
        
    def dump (self):
        return (BeautifulSoup(self.raw_string,features="lxml").prettify())

if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    parser = argparse.ArgumentParser(prog='KT_ROM')
    parser.add_argument('--update',metavar='USER', help='dump and update')
    args = parser.parse_args()
    user = args.update
    
    cache_file = "./cache/" + user + ".html"
    yaml_file = "./tweets/" + user + ".yaml"
    
    if not os.path.exists(cache_file):
        f=open(cache_file,"w",newline="\n")
        f.close()
    if not os.path.exists(yaml_file):
        f=open(yaml_file,"w",newline="\n")
        f.close()
    
    def cache_to_yaml():
        f=open(yaml_file,"r")
        l=f.readline()
        max_id_current=None
        while l:
            if l.startswith("id:"):
                max_id_current = int(l.replace("id: ",""))
                break
            l=f.readline()
        f.close()

        a=BigA()
        if max_id_current:
            #print("FAQ")
            #exit()
            aa=a.from_file(cache_file,min_id=max_id_current)
        else:
            if os.path.getsize(cache_file)>1:
                aa=a.from_file(cache_file)
            else:
                aa=[]
            pass
        #print(aa[-1].id)
        s=""
        for x in aa:
            s=s+x.dump_yml()
        if s:
            f=open(yaml_file,"r")
            s0=f.read()
            f.close()
            f=open(yaml_file,"w")
            f.write(s+s0)
            f.close()
    
    def make_cache():
        a=BigA()
        a.fetch(user)
        f=open(cache_file,"w",newline="\n")
        f.write(a.raw_string)
        f.close()
    
    cache_to_yaml()
    make_cache()
    cache_to_yaml()
    exit()
    
